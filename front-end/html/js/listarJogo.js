(function(){
    const jogos = document.querySelector('.jogos')

    function createTr(nomeJogo, propriedades){
        const quantidadePropriedades = propriedades.length
        const jogo = document.createElement('article')
        let listItems='';
        for(let i = 0; i < quantidadePropriedades; i++){
            listItems += `<li class="jogo-card__list-item">${propriedades[i]}</li>`
        }
        jogo.classList.add('jogo-card')
        jogo.innerHTML = `
        <h2 class="jogo-card__title">${nomeJogo}</h2>
        <div class="jogo-card__content">
            <ul class="jogo-card__lista">
                ${listItems}
            </ul>
        </div>
            ` 
        jogos.appendChild(jogo)
    }

    async function listarJogos(){
        const request = await fetch('http://localhost:3000/api/jogos')
        const response = await request.json()
        for(jogo of response){
            createTr(jogo.nomeJogo, jogo.propriedades)
        }
    }
    window.addEventListener('load', listarJogos)
})()


