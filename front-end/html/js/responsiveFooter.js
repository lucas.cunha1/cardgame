  const campoFooter = document.querySelector(".footer-fixed");

  function responsiveFooter() {
    const alturaJanela = window.innerHeight;
    const alturaSite = document.body.clientHeight + campoFooter.clientHeight;
    if (alturaJanela <= alturaSite) {
      mudarClassParaFooter();
    } else {
      mudarClassParaFooterFixed();
    }
  }

  function mudarClassParaFooterFixed() {
    try {
      let footer = document.querySelector(".footer");
      footer.classList.remove("footer");
      footer.classList.add("footer-fixed");
    } catch (err) {
      console.log(err);
    }
  }

  function mudarClassParaFooter() {
    try {
      let footer = document.querySelector(".footer-fixed");
      footer.classList.remove("footer-fixed");
      footer.classList.add("footer");
    } catch (err) {
      console.log(err);
    }
  }

  window.addEventListener("resize", responsiveFooter);

responsiveFooter()
