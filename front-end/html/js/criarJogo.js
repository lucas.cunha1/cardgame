(function(){

    let propriedadesJaForamCriadas = false;
    const campoQuantidadePropriedades = document.querySelector('#quantidadePropriedades')
    campoQuantidadePropriedades.addEventListener('input', (event) => {
        let quantidadePropriedades = campoQuantidadePropriedades.value
        if(quantidadePropriedades < 6 && quantidadePropriedades > 1){
            adicionarCamposPropriedade()
            propriedadesJaForamCriadas = true
        }
        
        function adicionarCamposPropriedade(){
            for(let i = quantidadePropriedades; i > 0; i--){        
                if(propriedadesJaForamCriadas){
                    removerCamposDasPropriedades()
                }
                console.log('entrou no for')
                let form = document.querySelector('.criar-jogo')
                let label = document.createElement('label')
                let div = document.createElement('div')
                div.classList.add('criar-jogo')
                div.classList.add('criar-jogo--dynamic')
                label.textContent = `Atributo ${i}`
                label.id = `labelPropriedade${i}`
                label.name = `labelPropriedade${i}`
                let input = document.createElement('input')
                input.placeholder = 'Nome do atributo. Exemplos: Ataque, altura, peso...'
                input.id = `inputPropriedade${i}`
                input.name = `propriedades[]`
                input.required = true;
                label.setAttribute('for', input.id)
                div.appendChild(label)
                div.appendChild(input)
                campoQuantidadePropriedades.after(div);
            }
            responsiveFooter()
            function removerCamposDasPropriedades(){
                let form = document.querySelector('.criar-jogo')
                let camposCriados = form.querySelectorAll('.criar-jogo')
                camposCriados.forEach((element) => {
                    form.removeChild(element)
                })
                propriedadesJaForamCriadas = false;
            }
        }
    })
})()
